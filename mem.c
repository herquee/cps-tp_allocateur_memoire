#include "mem.h"
#include "common.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>


// constante définie dans gcc seulement
#ifdef __BIGGEST_ALIGNMENT__
#define ALIGNMENT __BIGGEST_ALIGNMENT__
#else
#define ALIGNMENT 16
#endif

/*#define ALIGN(val, align)\
	(((__intptr_t)(val) + ((align) - 1))&~((align) - 1))*/

struct fb {
	size_t size;
	struct fb* next;
	int busy;
};

typedef struct {
	size_t size;
	size_t sizeMeta;
	void *adress;
}memoire;

memoire memGlobal;

void mem_init(void* mem, size_t taille)
{
	assert(mem == get_memory_adr());
	assert(taille == get_memory_size());

	memGlobal.size = taille;
	memGlobal.sizeMeta = sizeof(struct fb);
	memGlobal.adress = mem;

	//zl
	struct fb *premier_fb = (struct fb*)memGlobal.adress;
	premier_fb->size = memGlobal.size - memGlobal.sizeMeta;
	premier_fb->next = NULL;
	premier_fb->busy = 0;

	mem_fit(&mem_fit_first);
}

void mem_show(void (*print)(void *zone, size_t size, int free))
{
	void *current = memGlobal.adress;
	while (current != NULL) {
		struct fb *bloc = (struct fb*)current;
		print(current, bloc->size + memGlobal.sizeMeta, !bloc->busy);
		current = bloc->next;
	}

}

static mem_fit_function_t *mem_fit_fn;
void mem_fit(mem_fit_function_t *f) {
	mem_fit_fn=f;
}

void *mem_alloc(size_t taille) {
	__attribute__((unused)) /* juste pour que gcc compile ce squelette avec -Werror */
	struct fb *zoneAlloc=mem_fit_fn((struct fb*)memGlobal.adress,taille);
	if (taille >= zoneAlloc->size){
		return NULL;
	}
	void *adresseZL = (void*)zoneAlloc + taille + memGlobal.sizeMeta ;
	struct fb *prochainLibre = (struct fb*)adresseZL;

	prochainLibre->size = zoneAlloc->size - taille - memGlobal.sizeMeta;
	prochainLibre->next = zoneAlloc->next;
	prochainLibre->busy = 0;
	zoneAlloc->size = taille;
	zoneAlloc->next = prochainLibre;
	zoneAlloc->busy = 1;

	return zoneAlloc;
}


void mem_free(void* mem) {
}


struct fb* mem_fit_first(struct fb *list, size_t size)
{
	struct fb *current = list;
	while(((void *)current) < (memGlobal.adress+memGlobal.size)){
		if(current->busy == 0)
			if(current->size >= size+memGlobal.sizeMeta)
				return current;
		current = current->next ;
	}
	printf("Error\n");
	return NULL;

}

/* Fonction à faire dans un second temps
 * - utilisée par realloc() dans malloc_stub.c
 * - nécessaire pour remplacer l'allocateur de la libc
 * - donc nécessaire pour 'make test_ls'
 * Lire malloc_stub.c pour comprendre son utilisation
 * (ou en discuter avec l'enseignant)
 */
size_t mem_get_size(void *zone)
{
	/* zone est une adresse qui a été retournée par mem_alloc() */
	struct fb *z = (struct fb*)zone;
	/* la valeur retournée doit être la taille maximale que
	 * l'utilisateur peut utiliser dans cette zone */
	return z->size;
}

/* Fonctions facultatives
 * autres stratégies d'allocation
 */
struct fb* mem_fit_best(struct fb *list, size_t size) {
	return NULL;
}

struct fb* mem_fit_worst(struct fb *list, size_t size) {
	return NULL;
}
